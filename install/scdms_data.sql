-- phpMyAdmin SQL Dump
-- version 3.3.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 28, 2013 at 03:03 PM
-- Server version: 5.1.54
-- PHP Version: 5.3.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: 'scdms'
--

--
-- Dumping data for table 'keys'
--

INSERT INTO `keys` (id, `key`, `level`, ignore_limits, date_created) VALUES
(3, 'F9C1852BB364751231C61CCDC6AB6', 0, 0, 0);

--
-- Dumping data for table 'keywords'
--

INSERT INTO keywords (id, `name`) VALUES
(55, 'securityguard'),
(56, 'jon'),
(57, 'samtarly'),
(58, 'kingofthenorth'),
(59, 'nolannister'),
(60, 'fornedstark'),
(61, 'war'),
(62, 'victory'),
(63, 'kinginthenorth'),
(64, 'BFF'),
(65, 'blonde'),
(66, 'xmas'),
(67, 'direwolf'),
(68, 'sword'),
(69, 'weapon'),
(70, 'longsword'),
(71, 'coat'),
(72, 'cold'),
(73, 'fighting'),
(74, 'onlytalent'),
(75, 'family'),
(76, 'stark'),
(77, 'sam'),
(78, 'onlyfriend');

--
-- Dumping data for table 'projects'
--

INSERT INTO projects (id, project_name, description, date_created, date_end, date_modified, `status`, assignee, date_assigned, last_modified_by) VALUES
(63, 'Guarding the wall', 'Thou shall not pass', '2013-11-28 14:07:11', '2013-11-30 00:00:00', '2013-11-28 14:08:59', 'on-progress', 4, '2013-11-28 14:08:59', 5),
(64, 'Preparing for my big wedding', 'Attend with any colour except red', '2013-11-28 14:11:41', '2013-11-13 00:00:00', '2013-11-28 14:11:41', 'pending', 5, '2013-11-28 14:11:41', 5),
(65, 'Surrender Kings Landing', 'King Joffrey must step down', '2013-11-28 14:14:42', '2013-12-31 00:00:00', '2013-11-28 14:14:42', 'new', 6, '2013-11-28 14:14:42', 5),
(66, 'Conspire with Tywin', 'My armies are getting small so must think of other way.', '2013-11-28 14:16:55', '2014-01-01 00:00:00', '2013-11-28 14:42:00', 'closed', 8, '2013-11-28 14:16:55', 8),
(67, 'Capture Jaime', 'Responsible for the capture of my dear father Ned so he must be captured as well', '2013-11-28 14:19:34', '2013-08-01 00:00:00', '2013-11-28 14:57:50', 'closed', 10, '2013-11-28 14:19:34', 8),
(68, 'Talk to Tyrion', 'He''s the only one who seems to understand me', '2013-11-28 14:20:56', '2013-11-29 00:00:00', '2013-11-28 14:20:56', 'on-progress', 7, '2013-11-28 14:20:56', 4),
(69, 'Sam needs to workout', 'Sam really needs to go to Watch Gymn', '2013-11-28 14:23:07', '2013-11-27 00:00:00', '2013-11-28 14:36:16', 'on-progress', 4, '2013-11-28 14:36:16', 4),
(70, 'Talk about her cheeckbone', 'I mean really?! Look at her cheekbone!', '2013-11-28 14:25:15', '2013-12-17 00:00:00', '2013-11-28 14:25:15', 'pending', 9, '2013-11-28 14:25:15', 4),
(71, 'Build an XMAS tree', 'Start decorating the North', '2013-11-28 14:27:00', '2013-12-25 00:00:00', '2013-11-28 14:27:00', 'new', 8, '2013-11-28 14:27:00', 4),
(72, 'Brush Snow''s fur', 'A bit tangled.', '2013-11-28 14:28:21', '2013-12-01 00:00:00', '2013-11-28 14:28:21', 'new', 4, '2013-11-28 14:28:21', 4),
(73, 'Sharpen ICE', 'It''s a big sword', '2013-11-28 14:29:59', '2013-11-28 00:00:00', '2013-11-28 14:29:59', 'on-progress', 4, '2013-11-28 14:29:59', 4),
(74, 'Buy new winter coat', 'Getting cold so need a new one', '2013-11-28 14:35:59', '2013-12-10 00:00:00', '2013-11-28 14:36:28', 'pending', 4, '2013-11-28 14:36:28', 4),
(75, 'Raid Kings Landing', 'Gather the men and prepare for war.', '2013-11-28 14:38:17', '2014-01-01 00:00:00', '2013-11-28 14:38:17', 'new', 0, NULL, 5),
(76, 'Greet my mom', 'All this fighting left no room for Mommy.', '2013-11-28 14:39:47', '2013-11-27 00:00:00', '2013-11-28 14:39:47', 'on-progress', 5, '2013-11-28 14:39:47', 5),
(77, 'Bromance with Sam', 'Sam is the best', '2013-11-28 14:41:00', '2013-11-27 00:00:00', '2013-11-28 14:41:00', 'closed', 4, '2013-11-28 14:41:00', 4);

--
-- Dumping data for table 'project_comments'
--

INSERT INTO project_comments (id, project_id, comments, user_id, `timestamp`) VALUES
(117, 63, 'Will do everything to protect Westeros', 4, '2013-11-28 14:07:11'),
(118, 63, 'Will do everything to protect Westeros. Forgot to add Shadow here.', 4, '2013-11-28 14:07:51'),
(119, 63, 'Will do everything to protect Westeros. Forgot to add Shadow here.\nHey Jon, Rob here!', 5, '2013-11-28 14:08:59'),
(120, 64, 'No Frays should be invited', 5, '2013-11-28 14:11:41'),
(121, 65, 'The current King is blonde but his father is brunette. Seems legit', 5, '2013-11-28 14:14:42'),
(122, 67, 'Captured! ', 5, '2013-11-28 14:19:34'),
(123, 68, 'Hi Tyrion.', 4, '2013-11-28 14:20:56'),
(124, 69, 'Still not being done!', 4, '2013-11-28 14:23:07'),
(125, 70, 'She''s not really into me', 4, '2013-11-28 14:25:15'),
(126, 71, 'Who will assists me?', 4, '2013-11-28 14:27:00'),
(127, 72, 'Give him a bath too.', 4, '2013-11-28 14:28:21'),
(128, 73, 'Still not sharp enough.', 4, '2013-11-28 14:29:59'),
(129, 74, 'I forgot I got no coins.', 4, '2013-11-28 14:35:59'),
(130, 69, 'Still not being done!', 4, '2013-11-28 14:36:16'),
(131, 74, 'I forgot I got no coins.', 4, '2013-11-28 14:36:28'),
(132, 75, 'Before the new year', 5, '2013-11-28 14:38:17'),
(133, 76, 'I forgot again.', 5, '2013-11-28 14:39:47'),
(134, 77, 'You''re my best friend Sam!', 4, '2013-11-28 14:41:00'),
(135, 66, 'No way!  -Tywin', 8, '2013-11-28 14:42:00');

--
-- Dumping data for table 'project_keywords'
--

INSERT INTO project_keywords (id, project_id, keyword_id) VALUES
(53, 63, 55),
(54, 63, 56),
(55, 63, 57),
(56, 64, 58),
(57, 64, 59),
(58, 65, 58),
(59, 65, 60),
(60, 65, 61),
(61, 66, 61),
(62, 67, 62),
(63, 67, 63),
(64, 68, 64),
(65, 69, 64),
(66, 70, 65),
(67, 71, 66),
(68, 72, 67),
(69, 73, 68),
(70, 73, 69),
(71, 73, 70),
(72, 74, 71),
(73, 74, 72),
(74, 75, 61),
(75, 75, 73),
(76, 75, 74),
(77, 76, 75),
(78, 76, 76),
(79, 77, 64),
(80, 77, 77),
(81, 77, 78);

--
-- Dumping data for table 'project_tags'
--

INSERT INTO project_tags (id, project_id, tag_id) VALUES
(54, 63, 18),
(53, 63, 14),
(52, 63, 15),
(51, 63, 16),
(50, 63, 17),
(55, 64, 19),
(56, 64, 20),
(57, 64, 21),
(58, 65, 22),
(59, 65, 23),
(60, 66, 24),
(61, 66, 25),
(62, 66, 26),
(63, 67, 27),
(64, 68, 28),
(79, 69, 30),
(78, 69, 29),
(67, 70, 31),
(68, 70, 32),
(69, 70, 33),
(70, 71, 34),
(71, 71, 26),
(72, 72, 18),
(73, 73, 16),
(74, 73, 35),
(75, 73, 36),
(81, 74, 38),
(80, 74, 37),
(82, 75, 39),
(83, 75, 22),
(84, 76, 40),
(85, 76, 41),
(86, 77, 29),
(87, 77, 42);

--
-- Dumping data for table 'tags'
--

INSERT INTO tags (id, `name`) VALUES
(14, 'guarding'),
(15, 'thewall'),
(16, 'ice'),
(17, 'scary'),
(18, 'shadow'),
(19, 'wedding'),
(20, 'excitedmuch'),
(21, 'talisa'),
(22, 'joffrey'),
(23, 'kingslanding'),
(24, 'urgent'),
(25, 'kingdom'),
(26, 'westeros'),
(27, 'enemy'),
(28, 'lonely'),
(29, 'sam'),
(30, 'fat'),
(31, 'queen'),
(32, 'regent'),
(33, 'jaime'),
(34, 'xmas'),
(35, 'valaryian'),
(36, 'sharp'),
(37, 'winter'),
(38, 'fashion'),
(39, 'war'),
(40, 'love'),
(41, 'mommy'),
(42, 'bestfriend');

--
-- Dumping data for table 'users'
--

INSERT INTO users (id, username, `password`, salt, role) VALUES
(4, 'jonsnow', 'c8d81a3206caf978e02e5b0ce2104537475bf927', 'ea183fb816b1459419f030b41d6dc109', 'manager'),
(5, 'robbstark', '46fa6fd172f6068824ee8d45d4ec41cecf8fa27b', '1613d969bc588d3dcf9ac837a760e07f', 'manager'),
(6, 'joffrey', 'f4b3ceba5b390f44d3bb26c18e8afd2cd1360bb8', 'cf2c616781ba805b0729c12fa397858f', 'content-producer'),
(7, 'tyrion', '02534f7b120955ae6cdf34fd083ba63af92b82de', '3806a382a39453266d5b57deff936840', 'content-producer'),
(8, 'tywin', '9bf554c07dfbcdeb270234e63274eb4bc5bc62fb', '77b1ad54576ee3e95c58716da32268f3', 'content-producer'),
(9, 'cersei', '3b9f18cbecf53d770e74847a0c6f83d045d9238d', '2615e6c183d5a9d6d558d50e18d5e6e1', 'content-producer'),
(10, 'jaime', '587305c75d28b2118b09af0dc1d46d854534bb90', '9836c655147a6b56e30ea84698cc6c12', 'content-producer');
