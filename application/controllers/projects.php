<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends SCDMS_Controller {
    public $data;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('project');
        $this->load->model('user');
        
        //load custom helpers
        $this->load->helper('view');
        
        //controller specific libaries
        $this->load->library("pagination");
        
        //some scope vars
        $this->data = array();
        $this->data['field_access'] = '';
        $this->data['role'] =  $this->session->userdata('role');
        
    }
    public function index()
    {
        //the flash messeage
        $this->data['message'] = $this->session->flashdata('message');
        
        //pagination
        $project_count = $this->project->count_all();
        $pagination_config = array(
            'base_url' => base_url().'projects/index',
            'total_rows' => $project_count,
            'per_page'   => 10,
        );
        $this->pagination->initialize($pagination_config);
        $page = ($this->uri->segment(3)) ? (int) $this->uri->segment(3) : 1;
        $page = ($page == 0)?1:$page;
        $this->data['pagination'] = $data["links"] = $this->pagination->create_links();
        
        //get the projects
        $this->data['projects'] = $this->project->get_projects($pagination_config['per_page'],($page - 1)*10);
        $this->data['content'] = 'projects/index';
        $this->load->view($this->layout,$this->data);
    }
    
    public function add(){
        $this->data['form_data'] = array();
        if($this->input->post()){
            if(!$this->project->is_valid($this->input->post())){
                $this->data['error_fields'] = $this->project->error_fields;
                $this->data['message'] = $this->project->messages['errors'];
                $this->data['form_data'] = $this->input->post();
            }else{
                if($this->project->save($this->input->post())){
                    $this->session->set_flashdata('message', 'Project added.');
                    redirect('projects');
                }
            }
        }
        
        //set assignees
        $users = $this->user->get_users();
        $this->data['users'] = $users;
        $this->data['content'] = 'projects/add';
        
        $this->data['page_title'] = 'Add New Project';
        
        $this->load->view($this->layout,$this->data);
    }
    
    public function edit(){
        //get id param
        $id = (int) ($this->uri->segment(3));
        
        //get the project data
        $project = $this->project->get_projects(1,'',array(
            'filter' => array(
                'id'=>array('p.id'=>$id)
            )
        ));
        
        if(empty($project)){
            redirect('projects');
        }
        
        $project = current($project);
        
        //project exists, then automatically record as an update
        $update_history = $this->project->save_update_history($project->id,$this->session->userdata('id'));
        
        //update the project modified dates
        $users = $this->user->get_users();
        $project->date_modified = $update_history['date_modified'];
        $project->last_modified_by = $this->user->get_user_name($users,$update_history['last_modified_by']);
        
        $this->data['project'] = $project;
        
        //update
        if($this->input->post()){
            $post = $this->input->post();
            $post['project_id'] = $id;
            //validate
            if(!$this->project->is_valid($post)){
                $this->data['error_fields'] = $this->project->error_fields;
                $this->data['message'] = $this->project->messages['errors'];
                $this->data['form_data'] = $post;
            }else{
                if($this->project->save($post)){
                    $this->session->set_flashdata('message', 'Project updated.');
                    redirect('projects/edit/'.$id);
                }
            }
            
        }
        
        $this->data['form_data'] = $this->project->process_form_data($project);
        
        
        $this->data['users'] = $users;
        
        $this->data['field_access'] = $this->authacl->_acl['field_access'];
        
        $this->data['content'] = 'projects/edit';
        
        //if there's a flash message
        $this->data['flash'] = $this->session->flashdata('message');
        
        $this->data['page_title'] = 'Edit Project';
        
        $this->load->view($this->layout,$this->data);
    }
}