<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Api extends REST_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('project');
        
    }
    public function project_get(){
        if(!$this->get('id')){  
            $this->response(NULL, 400);  
        }
  
        $project = $this->project->get_projects(1,'',array('filter' => array('id'=>array('p.id'=>$this->get('id')))));  
          
        if($project){  
            $this->response($project, 200);
        }else{  
            $this->response(NULL, 404);  
        } 
    }
    
    public function projects_get(){
        //defaults
        $limit = false;
        $offset = '';
        
        //check for filters
        $options = array();
        
        //accepted date fields
        $date_fields = array(
            'date_created',
            'date_end',
            'date_modified'
        );
        
        //check for date searches
        if($this->get('search')){
            if(in_array($this->get('search'), $date_fields)){
                $start = $this->get('start');
                $end = $this->get('end');
                if($start && $end){
                    $options['filter']['date'][$this->get('search')]=array(
                        'start'=>date('Y-m-d', strtotime($start)),
                        'end'=>date('Y-m-d', strtotime($end))
                    );
                }
            }
        }
        
        if($this->get('limit')){
            $limit = $this->get('limit');
        }else{
            $limit = false; //fetch all
        }
        
        if($this->get('offset')){
            $offset = $this->get('offset');
        }
        
        $projects = $this->project->get_projects($limit,$offset,$options);
        $response['rows'] = $projects;
        $response['total_rows'] = count($projects);
        
        
       $this->response($response);
        
        
    }
} 