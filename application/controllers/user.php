<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library("authacl");
    }
    public function index()
    {
        redirect('/');
    }
    
    public function login(){
        $data = array();
        //check if logged in
        if($this->authacl->is_loggedin())
        {
            redirect("/projects/index");
        }
        
        if($this->input->post()){
            if(!$this->authacl->login($this->input->post())){
                $data['message'] = 'Wrong username and password combination.';
            }else{
                redirect('/projects/index');
            }
        }
        
        $this->load->view('user/login',$data);
    }
    
    public function logout(){
        return $this->authacl->logout();
    }
}