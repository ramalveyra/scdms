<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function format_keywords($keywords){
    if(count($keywords) > 0){
        
        foreach($keywords as $keyword){
            $tmp[] = $keyword->name;
        }
        $keywords = implode($tmp,', ');
    }else{
        $keywords = '';
    }
    return $keywords;
}

function format_tags($tags){
    if(count($tags) > 0){
        
        foreach($tags as $tag){
            $tmp[] = $tag->name;
        }
        $tags = implode($tmp,', ');
    }else{
        $tags = '';
    }
    return $tags;
}

function format_date($date, $format = 'default'){
    if($format == 'default'){
        $format = 'm-d-Y';
    }
    return date($format, strtotime($date));
    
}

function is_due($date, $status, $range = 'default'){
    if($range == 'default'){
        $range = date('Y-m-d H:i:s');
    }
    return ($date < $range && $status !=='closed' )?true:false;
}

function format_status($status){
    if($status=='new'){
        $label = 'label-info';
    }
    if($status=='on-progress'){
        $label = 'label-success';
    }
    if($status=='pending'){
        $label = 'label-warning';
    }
    if($status=='closed'){
        $label = 'label-closed';
    }
    return '<span class="label '.$label.'">'.ucfirst($status).'</span>';
}

function load_post_data($data, $key){
    if(isset($data)){
        if(is_array($data)){
            if(isset($data[$key])){
                return $data[$key];
            }
        }
    }
    return '';
}

function check_field_access($field_access, $field, $role){ 
    if(isset($field_access) && is_array($field_access)){
        if(array_key_exists($role, $field_access)){
            $access_flag = false;
            foreach($field_access[$role] as $field_name => $has_access){
                if($field_name == $field){
                    if($has_access){
                        $access_flag = true;
                        return '';
                    }
                }
            }
            if(!$access_flag){
                return 'disabled';
            }
        }
    }
}