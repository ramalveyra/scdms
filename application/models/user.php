<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model {
    
    public function get_users(){
        $this->db->select('id, username');
        $query = $this->db->get('users');
        return $query->result();
    }
    
    public function get_user_name($data, $id){
        if($data !== null){
            foreach($data as $user){
                if($user->id == $id){
                    return $user->username;
                }
            }
        }
    }
}