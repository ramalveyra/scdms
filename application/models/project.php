<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Model {
    public $messages;
    public $error_fields;
    
    
    
    public function get_projects($limit = 10, $offset = 0, $options = array()){
        if(isset($options['filter'])){
            if(is_array($options['filter'])){
                if(array_key_exists('id', $options['filter'])){
                    $limit = 1;
                    $this->db->where($options['filter']['id']);
                }
                //accepted date fields
                $project_date_fields = array(
                    'date_created',
                    'date_end',
                    'date_modified'
                );
                if(array_key_exists('date', $options['filter'])){
                    if(in_array(key($options['filter']['date']), $project_date_fields))
                    $field = key($options['filter']['date']);        
                    $this->_get_date_filters($field, 
                            $options['filter']['date'][$field]['start'], 
                            $options['filter']['date'][$field]['end']);
                    
                }
            }
        }else{
            $this->db->order_by('p.date_created','desc');
        }
        
        //get the projects
        if(!$limit == null){
            $this->db->limit($limit,$offset);
        }
        $query = $this->db->select(
           'p.id, 
            p.project_name, 
            p.description, 
            p.date_created,
            p.date_end,
            p.date_modified,
            p.status,
            p.date_assigned,
            u.username as assignee,
            u.id as assignee_id,
            u2.username as last_modified_by')
                ->from('projects as p')
                ->join('users as u','p.assignee = u.id','left')
                ->join('users as u2','p.last_modified_by = u2.id','left')
                ->get();
        
        $result = $query->result();
        
        //add related data
        if(count($result) > 0){
            foreach($result as $key=>$item){
                //get comments
                $result[$key]->comments = $this->get_project_comments($item->id,array('latest'=>true));
                //get tags
                $result[$key]->tags = $this->get_project_tags($item->id);
                //get keywords
                $result[$key]->keywords = $this->get_project_keywords($item->id);
            }
        }
        return $result;
    }
    
    public function get_project_comments($id, $options = array()){
        $id = (int) $id;
        
        //base query
        $this->db->select('*');
        //options
        if(isset($options['latest'])&& $options['latest'] == true){
            $this->db->order_by("timestamp", "desc")->limit(1);
        }
        $this->db->from('project_comments')
        ->where(array('project_id' => $id));
        $query = $this->db->get();
        
        return $result = $query->result();
    }
    
    public function get_project_tags($id){
        $id = (int) $id;
        $query = $this->db->select(
                't.name, pt.id as project_tag_id')
                ->from('project_tags as pt')
                ->join('tags as t','pt.tag_id = t.id')
                ->where(array('pt.project_id'=>$id))
                ->get();
        
        return $query->result();
    }
    
    public function get_project_keywords($id){
        $id = (int) $id;
        $query = $this->db->select(
                'k.name, pk.id as project_keyword_id')
                ->from('project_keywords as pk')
                ->join('keywords as k','pk.keyword_id = k.id')
                ->where(array('pk.project_id'=>$id))
                ->get();
        return $query->result();
    }
    
    public function get_project_by_id($id){
        $id = (int) $id;
        $query = $this->db->get_where('projects',array('id' => $id),1);
        return $query->result();
    }
    
    public function count_all(){
        return $this->db->count_all("projects");
    }
    
    //the validation class
    public function is_valid($data){
        //restrict the data
        $data = $this->project->filter_post_data($this->authacl->_acl['field_access'], $data, $this->session->userdata('role'));
        $is_valid = true;
        $messages = array(
            'required'=>'Please supply required fields.',
            'date' => 'Invalid date.'
        );
        
        $rules = array(
            'project_name' => array('required'),
            'description' => array('required'),
            'date_deadline' => array('required', 'date'),
            'status' => array('required'),
        );
        
        foreach($data as $key => $val){
           if(array_key_exists($key, $rules)){
               foreach($rules[$key] as $rule){
                   if($rule == 'required'){
                       if($val == ''){
                           if(!isset($this->messages['errors']) || !in_array($messages['required'], $this->messages['errors'])){
                                $this->messages['errors'][] = $messages['required'];
                           }
                           $this->error_fields[] = $key;
                           $is_valid = false;
                       }
                   }
                   
                   if($rule == 'date' && $val!==''){
                       $date = $val;
                       $d = DateTime::createFromFormat('m/d/Y', $date);
                       if(!($d && $d->format('m/d/Y') == $date)){
                           if(!isset($this->messages['errors']) || !in_array($messages['date'], $this->messages['errors'])){
                                $this->messages['errors'][] = $messages['date'];
                           }
                           $this->error_fields[] = $key;
                           $is_valid = false;
                       }
                       
                   }
               }
           } 
        }
        return $is_valid;
    }
    
    public function process_form_data($data){
        $form_data = array(
            'id' => $data->id,
            'project_name'=>$data->project_name,
            'description'=> $data->description,
            'date_deadline'  => date('m/d/Y', strtotime($data->date_end)),
            'status' => $data->status,
            'comments' => count($data->comments) > 0?$data->comments[0]->comments:'',
            'assignee' => $data->assignee_id
        );
        
        //for keywords and tags
        $this->load->helper('view');
        $form_data['keywords'] = format_keywords($data->keywords);
        $form_data['tags'] = format_tags($data->tags);
        
        return $form_data;
    }
    
    public function filter_post_data($field_access, $data, $role){
        $filtered_data = array();
        foreach($data as $key=>$value){
            if(array_key_exists($role, $field_access)){
                foreach($field_access[$role] as $field_name => $has_access){
                    if($key == $field_name){
                        if($has_access){
                            $filtered_data[$field_name]=$value;
                        }
                    }
                }
            }else{
                return $data;
            }
        }
        return $filtered_data;
    }
    
    public function save($data){
        //set fields
        $current_datetime = date('Y-m-d H:i:s');
        
        $project = array(
            'project_name' => isset($data['project_name'])?$data['project_name']:null,
            'description' => isset($data['description'])?$data['description']:null,
            'date_end'  =>  isset($data['date_deadline'])?date('Y-m-d', strtotime($data['date_deadline'])):null,
            'status'    => isset($data['status'])?$data['status']:null,
            'assignee'  => isset($data['assignee'])?$data['assignee']:null
        );
        
        if($project['assignee']!=='' && $project['assignee']!==null){
             $project['date_assigned'] = $current_datetime;
        }
        
        //apply restriction to items to save
        $project = $this->project->filter_post_data($this->authacl->_acl['field_access'], $project, $this->session->userdata('role'));
        
        //default project fields
        
        $project['date_modified'] = $current_datetime;
        $project['last_modified_by'] = $this->session->userdata('id');
        
        if(isset($data['project_id'])){
            $project_id = $data['project_id'];
            //update the project
            $this->db->where('id', $project_id);
            $this->db->update('projects', $project);
        }else{
            $project['date_created'] = $current_datetime;
            $this->db->set($project);
            $this->db->insert('projects');
            $project_id = $this->db->insert_id();
        }
        
        //apply field restrictions
        $data = $this->project->filter_post_data($this->authacl->_acl['field_access'], $data, $this->session->userdata('role'));
        
        //check for comments
        if(isset($data['comments']) && $data['comments']!==''){
            //save comments
            $comments = array(
                'project_id' => $project_id,
                'user_id' => $this->session->userdata('id'),
                'timestamp' => $current_datetime,
                'comments' => $data['comments']
            );
            
            $this->_save_project_comments($comments);
        }
        
        //check for keywords
        if(isset($data['keywords'])){
            $keywords = explode(',',$data['keywords']);
            //get project keywords first
            $project_keywords = $this->project->get_project_keywords($project_id);
            //delete if not in new set of keywords
            if(count($project_keywords) > 0){
                foreach($project_keywords as $keyword){
                    if(!in_array($keyword->name, $keywords)){
                        //delete the keyword;
                        $this->db->delete('project_keywords', array('id' => $keyword->project_keyword_id)); 
                    }
                }
            }
            
            foreach ($keywords as $keyword) {
                $data_item = array(
                    'name' => $keyword
                );
                
                $keyword_id = $this->_save_keywords($data_item);
                if($keyword_id !== 0){
                    //map keyword to project
                    $project_keyword = array(
                        'project_id' => $project_id,
                        'keyword_id' => $keyword_id
                    );
                    $this->_save_project_keywords($project_keyword);
                }else{
                    //check if project has keyword
                    $keyword = $this->_get_project_keyword_by_name($project_id, $keyword);
                    if(!empty($keyword)){
                        if($keyword[0]->project_id == null){
                            $project_keyword = array(
                                'project_id' => $project_id,
                                'keyword_id' => $keyword[0]->keyword_id
                            );
                            $this->_save_project_keywords($project_keyword);
                        }
                    }
                }
            }
            
        }
        
        //check for tags
        if(isset($data['tags']) && $data['tags']!==''){
            $tags = explode(',',$data['tags']);
            //get project tags first
            $project_tags = $this->project->get_project_tags($project_id);
            //delete if not in new set of keywords
            if(count($project_tags) > 0){
                foreach($project_tags as $tag){
                    if(!in_array($keyword->name, $tags)){
                        //delete the keyword;
                        $this->db->delete('project_tags', array('id' => $tag->project_tag_id)); 
                    }
                }
            }
            foreach ($tags as $tag) {
                $data_item = array(
                    'name' => $tag
                );
                
                $tag_id = $this->_save_tags($data_item);
                if($tag_id !== 0){
                    //map keyword to project
                    $project_tag = array(
                        'project_id' => $project_id,
                        'tag_id' => $tag_id
                    );
                    $this->_save_project_tags($project_tag);
                }else{
                    //check if project has tag
                    $tag = $this->_get_project_tag_by_name($project_id, $tag);
                    if(!empty($tag)){
                        if($tag[0]->project_id == null){
                            $project_tag = array(
                                'project_id' => $project_id,
                                'tag_id' => $tag[0]->tag_id
                            );
                            $this->_save_project_tags($project_tag);
                        }
                    }
                }
            }
            
        }
       return true;
    }
    
    public function save_update_history($project_id,$user_id){
        $project_id = (int) $project_id;
        $user_id = (int) $user_id;
        $data = array(
            'date_modified' => date('Y-m-d H:i:s'),
            'last_modified_by' => $user_id
         );
         $this->db->where('id', $project_id);
         $this->db->update('projects', $data); 
         return $data;
    }
    
    protected function _save_project_comments($data){
        $this->db->set($data);
        return $this->db->insert('project_comments');
    }
    
    protected function _save_keywords($data){
        //save and prevent duplicates
        $insert_query = $this->db->insert_string('keywords', $data);
        $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
        $this->db->query($insert_query);
        return $this->db->insert_id();
    }
    
    protected function _save_project_keywords($data){
        $this->db->set($data);
        return $this->db->insert('project_keywords');
    }
    
    protected function _save_tags($data){
        //save and prevent duplicates
        $insert_query = $this->db->insert_string('tags', $data);
        $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
        $this->db->query($insert_query);
        return $this->db->insert_id();
    }
    
    protected function _save_project_tags($data){
        $this->db->set($data);
        return $this->db->insert('project_tags');
    }
    
    protected function _get_project_keyword_by_name($project_id,$name){
        $project_id = (int) $project_id;
        $query = $this->db->select(
                'pk.id,
                 pk.project_id,
                 pk.keyword_id,
                 k.name,
                 k.id as keyword_id')
            ->from('project_keywords as pk')
            ->join('keywords as k',"pk.keyword_id = k.id AND pk.project_id = $project_id",'right')
            ->where(array('k.name'=>$name))
            ->limit(1)    
            ->get();
        return $query->result();
    }
    protected function _get_project_tag_by_name($project_id,$name){
        $project_id = (int) $project_id;
        $query = $this->db->select(
                'pt.id,
                 pt.project_id,
                 pt.tag_id,
                 t.name,
                 t.id as tag_id')
            ->from('project_tags as pt')
            ->join('tags as t',"pt.tag_id = t.id AND pt.project_id = $project_id",'right')
            ->where(array('t.name'=>$name))
            ->limit(1)    
            ->get();
        return $query->result();
    }
    
    /* generates date filters */
    protected function _get_date_filters($field, $start_date, $end_date){
        $date_range = "p.$field BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'";
        $this->db->where($date_range, NULL, FALSE);
        $this->db->order_by("p.$field",'desc');
    }

}
