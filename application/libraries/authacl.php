<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class for handling authentication and basic acl
 * Author: Ram A.
 */

class Authacl
{
    public $CI;
    public $_acl = array();
    
    public function __construct()
    {
        $this->CI = & get_instance();
        
        // dependencies
        $this->CI->load->database();
        $this->CI->load->library("session");
        $this->CI->load->helper('url');
        
        //acl
        $this->_acl = array(
            'roles' => array(
                'content-producer' => array(
                    'projects' => array(
                        'add' => false
                    )
                )
            ),
            //set field access depending on role
            'field_access' => array(
                'content-producer' => array(
                    'comments' => true,
                    'status'   => true 
                )
            )
        );
    }
    
    public function is_loggedin() {
        return ($this->CI->session->userdata("id")) ? true : false;
    }
    
    public function login($post){
        if($post){
            $username = $post['username'];
            $password = $post['password'];
            
            //authenticate
            $auth_sql = 'SELECT * FROM users where username = ? AND password = SHA1(CONCAT(?,salt))';
            $query = $this->CI->db->query($auth_sql,array($username,$password));
            
            if($query->num_rows() == 1)
            {
                //save to session
                $user_data = array(
                    'id' => $query->row()->id,
                    'username' => $query->row()->username,
                    'role' => $query->row()->role
                );
                
                $this->CI->session->set_userdata($user_data);
                
                return true;
            }
        }
        return false;
    }
    
    // do logout
    public function logout() {
        $this->CI->session->sess_destroy();
        redirect('/user/login');
    }
    
    //check if user has permissions
    /**
     * * @param string $role
     * * @param array $route
     */
    public function has_permission($role,$route){
        
        //search through array
        if(array_key_exists($role,$this->_acl['roles'])){
            //if role found, search for class
            if(array_key_exists($route['controller'], $this->_acl['roles'][$role])){
                //if class is found, check action
                if(array_key_exists($route['action'],$this->_acl['roles'][$role][$route['controller']])){
                    return $this->_acl['roles'][$role][$route['controller']][$route['action']];
                }
            }
        }
        //defaults to 'true' - all allowed
        return true;
    }
}