<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Custom class handling navigation and breadcrumbs
 * Author: Ram A.
 */

class Navigation
{
    public $pages;
    
    private $_current_controller;
    private $_current_action;
    
    public $nav_arr = array(
        'default' => array(
            'pages' => array(
                'page1'=>array(
                    'label' => 'Home',
                    'controller' => '/',
                    'action' => '',
                    'order'=>1,
                    'pages'=>array(
                        'page1_1'=>array(
                            'label' => 'Project Management',
                            'controller'=>'projects',
                            'action'=>'index',
                            'order'=>2,
                            'pages' => array(
                                'page1_1_1'=>array(
                                    'label' =>'Add',
                                    'controller' => 'projects',
                                    'action' => 'add',
                                    'order' => 3
                                ),
                                'page1_1_2'=>array(
                                    'label' =>'Edit',
                                    'controller' => 'projects',
                                    'action' => 'edit',
                                    'order' => 4
                                )
                            )
                        )
                    )
                )
            )
        )
    );
    
    function __construct($pages = null) {
        $router =& load_class('Router', 'core');
        $this->_current_controller = $router->fetch_class();
        $this->_current_action = $router->fetch_method();
        
        if(isset($pages) && $pages !==null){
            if(array_key_exists($pages['pages'], $this->nav_arr)){
                $this->pages = $this->nav_arr['default']['pages'];
            }
        }
        
    }
    
    public function breadcrumb(){
        $breadcrumb = '<ul class="breadcrumb">';
        $breadcrumb.= $this->_generate_menus($this->pages);
        $breadcrumb.='</ul>';
        
        return $breadcrumb;
    }
    
    protected function _generate_menus($pages){
        static $menu = '';
        static $active = false;
        foreach ($pages as $page){
            if($page['controller']==$this->_current_controller && $page['action']==$this->_current_action){
                $active = true;
                 return $menu.='<li class="active">'.$page['label'];
            }else{
                $item = 0;
                foreach($pages as $tmp){

                    if($tmp['controller']==$this->_current_controller){
                        $item+=1;
                    }
                }
                if($item<=1 && $active == false){
                $menu.='<li><a href="'.base_url($page['controller'].DIRECTORY_SEPARATOR.$page['action']).'">'.$page['label'].'</a><span class="divider">&gt;</span>';
                }
            }
            if(!empty($page['pages'])){
               
              $this->_generate_menus($page['pages']);
            }
            $menu.='</li>';
        }
        return $menu;
        
    }
}