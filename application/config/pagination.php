<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
'full_tag_open' => '<ul>',
'full_tag_close' => '</ul>',
'num_tag_open' => '<li>',
'num_tag_close' => '</li>',
'first_tag_open'=>'<li>',
'first_link' => 'First',
'first_tag_close'=>'</li>',
'last_tag_open'=>'<li>',
'last_link' => 'Last',
'last_tag_close'=>'</li>',
'next_tag_open'=>'<li>',
'next_link'=>'&gt;',
'next_tag_close'=>'</li>',
'prev_tag_open'=>'<li>',
'prev_link'=>'&lt;',
'prev_tag_close'=>'</li>',
'cur_tag_open' =>'<li class="active"><a>',
'cur_tag_close' =>'</a></li>',
'use_page_numbers' => true 
);