<!DOCTYPE html>
<html>
  <head>
    <title>Site Content Data Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('public/assets/library/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('public/assets/css/style.css')?>" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('public/assets/css/flick/jquery-ui-1.10.3.custom.min.css')?>" rel="stylesheet" media="screen">
    <script src="<?php echo base_url('public/assets/js/jquery/jquery-1.10.2.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/js/jquery/jquery-ui-1.10.3.custom.min.js')?>"></script>
  </head>
  <body>
    <div id="wrap">  
        <div class="navbar navbar-inverse">                     
            <div class="navbar-inner">
                <div class="container-fluid">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="<?php echo site_url();?>">Site Content Data Management System</a>
                    <div class="nav-collapse collapse">
                    <p class="navbar-text pull-right">
                        Logged in as: <span class="logged-user"><?php echo $this->session->userdata('username');?></span> <small><a href="<?echo site_url('user/logout');?>" class="nav-link">[sign out]</a></small> 
                    </p>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->navigation->breadcrumb();?>
        <div id="main" class="container-fluid">
            <?php $this->load->view($content); ?>
        </div>
        <div id="push"></div>
    </div>
    <div id="footer">
      <div class="container">
          <p class="text-center">&copy; Copyright <?echo date('Y');?> Site Content Data Management System</p>
      </div>
    </div>
    <script src="<?php echo base_url('public/assets/library/bootstrap/js/bootstrap.min.js')?>"></script>
  </body>
</html>