<?php if(isset($message) && $message):?>
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $message;?>
</div>
<?php endif;?>
<?php if(!isset($projects) || count($projects) == 0):?>
<div class="alert alert-info">
    There are no projects to display.
</div>
<?php if($this->session->userdata('role') == 'manager'):?>
    <p>&nbsp;<i class="icon-briefcase"></i> <a href="<?php echo site_url('projects/add')?>">Add new project</a></p>
<?php endif; ?>
<?php else:?>
<?php if($this->session->userdata('role') == 'manager'):?>
    <p>&nbsp;<i class="icon-briefcase"></i> <a href="<?php echo site_url('projects/add')?>">Add new project</a></p>
<?php endif; ?>    
<div class="row-fluid">
    <div class="span12">
        <?php if(isset($projects) || count($projects) > 0):?>
            <table class="table table-bordered table-hover project-details table-striped">
                <thead>
                    <th>Project name</th>
                    <th>Description</th>
                    <th>Date created</th>
                    <th>Deadline date</th>
                    <th>Assignee</th>
                    <th>Date assigned</th>
                    <th>Status</th>
                    <th>Comments</th>
                    <th>Keywords</th>
                    <th>Tags</th>
                    <th>Date modified</th>
                    <th>Modified by</th>
                </thead>
                <tbody>
                    <?php foreach($projects as $project):?>
                    <tr class="project-<?php echo $project->id?>">
                        <td class="project_name"><?php echo $project->project_name ?></td>
                        <td class="description"><?php echo $project->description ?></td>
                        <td class="date_created"><?php echo format_date($project->date_created) ?></td>
                        <td class="date_end">
                            <?php if(is_due($project->date_end, $project->status)):?>
                            <span style="color: red"><?php echo format_date($project->date_end)?></span>
                            <?php else:?>
                            <?php echo format_date($project->date_end)?>
                            <?php endif;?>
                        </td>
                        <td class="assignee"><?php echo ($project->assignee == null)?"unassigned":$project->assignee ?></td>
                        <td class="date_assigned"><?php echo ($project->date_assigned == null)?'unassigned': format_date($project->date_assigned) ?></td>
                        <td class="status"><?php echo format_status($project->status) ?></td>
                        <td class="comments"><?php echo count($project->comments) > 0?$project->comments[0]->comments:''?></td>
                        <td class="keywords"><?php echo format_keywords($project->keywords);?></td>
                        <td class="tags"><?php echo format_keywords($project->tags);?></td>
                        <td class="date_modified"><?php echo ($project->date_modified == null)?'': format_date($project->date_modified,'m-d-Y H:i:s') ?></td>
                        <td class="last_modified_by"><?php echo $project->last_modified_by ?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <div class="pagination">
            <?php echo $pagination;?>
            </div>
            <script type="text/javascript">
            $(document).ready(function () {
                $('.project-details tbody tr').click(function(){
                   var id = $(this).attr('class');
                   id = id.replace(/^\D+/g, '');
                   var url = '<?php echo base_url('projects/edit/')?>';
                   window.location.href = url+'/'+id;
                });
            });
            </script>
        <?php endif;?>
            
    </div>
</div>
<?php endif;?>