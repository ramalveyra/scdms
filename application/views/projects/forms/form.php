<div class="row-fluid">
    <div class="span8 offset2">
        <h4><?php echo $page_title; ?></h4><br/>
        <?php if(isset($flash) && $flash):?>
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $flash;?>
        </div>
        <?php endif;?>
        <form class="form-horizontal"  enctype="application/x-www-form-urlencoded" method="post">
            <?php if(isset($message)):?>
            <div class="alert alert-error">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <strong>Error saving Project.</strong><br/>
              <?php foreach($message as $key => $error):?>
                  <?php echo $error.'<br />'; ?>
              <?php endforeach;?>
            </div>
            <?php endif;?>
            <div class="form-wrapper">
                <div class="control-group">
                    <div class="controls">

                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="project_name">Project Name</label>
                    <div class="controls">
                    <input type="text" id="project_name" name="project_name" 
                           class="span8" 
                           value="<?php echo load_post_data($form_data,'project_name')?>"
                           <?php echo check_field_access($field_access,'project_name',$role)?>>
                    <small><span><em>* required</em></span></small>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="description">Description</label>
                    <div class="controls">
                        <textarea rows="3" id="description" name="description" class="span8" <?php echo check_field_access($field_access,'description',$role)?>><?php echo load_post_data($form_data,'description')?></textarea>
                        <small><span><em>* required</em></span></small>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="date-created">Deadline Date</label>
                    <div class="controls">
                        <input type="text" id="date_deadline" name="date_deadline" class="input-xlarge" 
                               value="<?php echo load_post_data($form_data,'date_deadline')?>"
                               <?php echo check_field_access($field_access,'date_deadline',$role)?>>
                        <small><span><em>* required</em></span></small>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="status">Status</label>
                    <div class="controls">
                    <select name="status" id="status" class="input-xlarge" <?php echo check_field_access($field_access,'status',$role)?>>
                        <?php $selected = load_post_data($form_data,'status')?>
                        <option value="new" <?php if($selected == 'new') echo 'selected=selected'?>>New</option>
                        <option value="on-progress" <?php if($selected == 'on-progress') echo 'selected=selected'?>>On-Progress</option>
                        <option value="pending" <?php if($selected == 'pending') echo 'selected=selected'?>>Pending</option>
                        <option value="closed" <?php if($selected == 'closed') echo 'selected=selected'?>>Closed</option>
                    </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="comments">Comments</label>
                    <div class="controls">
                    <textarea rows="5" id="comments" name="comments" class="span8" <?php echo check_field_access($field_access,'comments',$role)?>><?php echo load_post_data($form_data,'comments')?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="tags">Tags</label>
                    <div class="controls">
                    <input type="text" id="tags" name="tags" class="input-xlarge" 
                           value="<?php echo load_post_data($form_data,'tags')?>"
                           <?php echo check_field_access($field_access,'tags',$role)?>
                           style="height:112px;">
                    <small><span><em>* separate tags with commas</em></span></small>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="keywords">Keywords</label>
                    <div class="controls">
                    <input type="text" id="keywords" name="keywords" class="input-xlarge" 
                           value="<?php echo load_post_data($form_data,'keywords')?>"
                           <?php echo check_field_access($field_access,'keywords',$role)?>
                           style="height:112px;">
                    <small><span><em>* separate keywords with commas</em></span></small>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="status">Assigned to</label>
                    <div class="controls">
                    <select name="assignee" id="assignee" class="input-xlarge" <?php echo check_field_access($field_access,'assignee',$role)?>>
                        <option value="">Select Assignee</option>
                        <?php if(!empty($users)):?>
                        <?php foreach($users as $user):?>
                        <option value="<?php echo $user->id;?>" <?php if($user->id == load_post_data($form_data,'assignee')) echo 'selected = selected'?>><?php echo $user->username;?></option>
                        <?php endforeach;?>
                        <?php endif;?>
                    </select>
                    </div>
                </div>
            </div><br />
            <p>
                <button type="submit" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn" onclick="javascript:window.location.href='<?php echo base_url('projects')?>'">Cancel</button>
            </p>
        </form>
    </div>
</div>
<script src="<?php echo base_url('public/assets/js/jquery/jquery.tagsinput.min.js')?>"></script>
<link href="<?php echo base_url('public/assets/css/jquery.tagsinput.css')?>" rel="stylesheet" media="screen">
<script type="text/javascript">
$(document).ready(function () {
    var error_fields = [<?php if(isset($error_fields)){echo "'" . implode("','", $error_fields) . "'";}?>];
    if(error_fields.length > 0){
        $.each(error_fields,function(key,val){
            $('#'+val).addClass('error-field');
        });
    }
    $('#date_deadline').datepicker();
    
    if(!$('#tags').is(":disabled")){
        $('#tags').tagsInput({'defaultText':''});
    }
    if(!$('#keywords').is(":disabled")){
        $('#keywords').tagsInput({'defaultText':''});
    }
    
});
</script>