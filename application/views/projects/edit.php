<?php
$this->view('projects/forms/form');
?>
<?php if(isset($project) || count($project) > 0):?>
<h4>Project details</h4>
<table class="table table-bordered table-striped">
    <thead>
        <th>Date created</th>
        <th>Deadline date</th>
        <th>Assignee</th>
        <th>Date assigned</th>
        <th>Status</th>
        <th>Comments</th>
        <th>Keywords</th>
        <th>Tags</th>
        <th>Date modified</th>
        <th>Modified by</th>
    </thead>
    <tbody>

        <tr class="project-<?php echo $project->id?>">
            <td class="date_created"><?php echo format_date($project->date_created) ?></td>
            <td class="date_end">
                <?php if(is_due($project->date_end, $project->status)):?>
                <span style="color: red"><?php echo format_date($project->date_end)?></span>
                <?php else:?>
                <?php echo format_date($project->date_end)?>
                <?php endif;?>
            </td>
            <td class="assignee"><?php echo ($project->assignee == null)?"unassigned":$project->assignee ?></td>
            <td class="date_assigned"><?php echo ($project->date_assigned == null)?'unassigned': format_date($project->date_assigned) ?></td>
            <td class="status"><?php echo format_status($project->status) ?></td>
            <td class="comments"><?php echo count($project->comments) > 0?$project->comments[0]->comments:''?></td>
            <td class="keywords"><?php echo format_keywords($project->keywords);?></td>
            <td class="tags"><?php echo format_keywords($project->tags);?></td>
            <td class="date_modified"><?php echo ($project->date_modified == null)?'': format_date($project->date_modified,'m-d-Y H:i:s') ?></td>
            <td class="last_modified_by"><?php echo $project->last_modified_by ?></td>
        </tr>

    </tbody>
</table>
<?php endif;?>