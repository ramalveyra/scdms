<!DOCTYPE html>
<html>
  <head>
    <title>Site Content Data Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('public/assets/library/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('public/assets/css/style.css')?>" rel="stylesheet" media="screen">
  </head>
  <body>
    <div class="container">
        <h3 class="text-center site-title"><span class="green-accent">[ Site Content</span> Data Management System ]</h3>
        <br />
        
        <form class="form-signin" enctype="application/x-www-form-urlencoded" method="post">
            <h3 class="form-signin-heading">Please sign in</h3>
            <?php if(isset($message)):?>
            <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $message;?>
            </div>
            <?php endif;?>
            <input type="text" class="input-block-level" name="username" id="username" placeholder="Username">
            <input type="password" class="input-block-level" name="password" id="password" placeholder="Password">
            <button class="btn btn-large btn-primary" type="submit">Login</button>
        </form>
    </div>
    <script src="<?php echo base_url('public/assets/js/jquery/jquery-1.10.2.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/library/bootstrap/js/bootstrap.min.js')?>"></script>
  </body>
</html>