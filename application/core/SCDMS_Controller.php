<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class extending base controller
 * Added method for Auth and ACL to check for every contoller
 * Auth and ACL via library/auth.php
 **/
class SCDMS_Controller extends CI_Controller
{
    public $layout;
    
    function __construct()
    {
        parent::__construct();
        
        //load the db
        $this->load->database();
        //load the auth library
        $this->load->library("authacl");
        
        if(! $this->authacl->is_loggedin())
        {
            redirect("user/login");
        }
        
        //check for acl
        $route  = array(
            'controller' => $this->router->class,
            'action'     => $this->router->method
        );
        
        if(!$this->authacl->has_permission($this->session->userdata('role'),$route)){
            //not allowed, redirect
            redirect('/');
        }
        
        //load the navigation / breadcrumb
        $this->load->library("navigation",array('pages'=>'default'));
        
        //set the default layout
        $this->layout = 'layouts/default';
    }
}