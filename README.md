# SITE CONTENT DATA MANAGEMENT SYSTEM

## GENERAL DESCRIPTION

### A Sample PHP App using Code Igniter

## INSTALLATION INSTRUCTIONS

### 1. System requirements

1. Apache server running on local machine
2. PHP 5.3 or higher
3. MySQL running and configured locally
4. Git

### 2. Installation

2.1 Getting the source files 

1. Clone the application repository on Bitbucket on your localhost public directory (htdocs etc.). The repository can be located at: https://bitbucket.org/creativeideaz/scdms

2.2 Setup and configure database for the application

1. Go to 'scdms/install/' and run the sql files located in that directory.

- files: scdms.sql (contains database structure), scdms_data.sql (contains dummy data)

2. Once the database has been successfully created and relevant data have been loaded to it, check the application database configuration ('scdms/application/config/database.php') to verify that the settings are properly configured.

- By default the application is configured to use 'root' with no password as database user. Change the necessary values on this config file and supply them with the one on your MySQL setup.
- If all configurations are correct, you should be able to run the application via http://localhost/scdms and be prompted with the login page.

## APPLICATION USAGE

### 1. Login credentials

- There are two types of users that can log in to the application, 'manager' and 'content-producer'. 
- The items that can be accessed in the application are controlled by these user roles.
    Below are a pre-defined users that can be used to access the application

    |Username  |  Password        |    Role             |         Item Access                                | 
    |----------|------------------|---------------------|----------------------------------------------------|
    |jonsnow   |  winteriscoming  |    manager          |   all
    |robbstark |  ilovetalisa     |    manager          |   all
    |joffrey   |  kingforever     |    content-producer |   view projects, edit projects (comments, status)
    |tyrion    |  bronnmance      |    content-producer |   view projects, edit projects (comments, status)
    |twyin     |  handforever     |    content-producer |   view projects, edit projects (comments, status)
    |cersei    |  secretlover     |    content-producer |   view projects, edit projects (comments, status)
    |jaime     |  iamsecretlover  |    content-producer |   view projects, edit projects (comments, status)

### 2. Pages

1. User login - sign in page

2. Project Management - Displays the list of available projects
- Shows 10 projects per page
- To edit a project, click the table row containing the public details.

3. Project creation (only available to managers) - creates new projects.

4. Project update page - (limited fields for content-producers) - page to update project details.

### 3. Added Feature

#### 1. REST APIs
- Application supports API calls to fetch projects.
- Projects can be fetched by ID, date_created, date_end (project deadline) and date_modified.
- The application uses API keys as authentication for fetching projects.

- Configured API Keys that can be used are as follows:

    `F9C1852BB364751231C61CCDC6AB6`

#### 1.2 Using the API

1. Fetch a project (by ID)

syntax: `http://localhost/scdms/api/project/id/{PROJECT ID}/?api_key={API KEY (see above for available keys)}`

ex: `http://localhost/scdms/api/project/id/77/?api_key=F9C1852BB364751231C61CCDC6AB6`

2. Fetch all projects

syntax: `http://localhost/scdms/api/projects/?api_key={API KEY}`

ex: `http://localhost/scdms/api/projects/?api_key=F9C1852BB364751231C61CCDC6AB6`

3. Fetch all projects by date

- available  date filter: 'date_created', 'date_end' (project deadline) and 'date_modified'
 
syntax: `http://localhost/scdms/api/projects/search/{DATE FILTER}/start/{START OF DATE RANGE}/end/{END OF DATE RANGE}/?api_key={API KEY}`

examples:

`http://localhost/scdms/api/projects/search/date_created/start/2013-11-27/end/2013-11-28/?api_key=F9C1852BB364751231C61CCDC6AB6`
`http://localhost/scdms/api/projects/search/date_end/start/2013-12-25/end/2014-01-31/?api_key=F9C1852BB364751231C61CCDC6AB6`
`http://localhost/scdms/api/projects/search/date_modified/start/2013-11-27/end/2013-11-28/?api_key=F9C1852BB364751231C61CCDC6AB6`